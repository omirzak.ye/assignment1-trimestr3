////////////////////////////////////////////////////////////////////////
////
//// Given is a producer-consumer szenario, where a producer reads in
//// tweets from a mockstream and a consumer is processing the
//// data. Your task is to change the code so that the producer as well
//// as the consumer can run concurrently
////
//
package main

import (
	"fmt"
	"time"
)
var ch chan *Tweet
func producer(stream Stream) {
	for {
		tweet, err := stream.Next()
		if err == ErrEOF {
			close(ch)
			return
		}
		ch <- tweet
	}
}

func consumer() {
	for  {
		t, err := <- ch
		if err{
			return
		}
		if t.IsTalkingAboutGo() {
			fmt.Println(t.Username, "\ttweets about golang")
		} else {
			fmt.Println(t.Username, "\tdoes not tweet about golang")
		}
	}
}

func main() {
	start := time.Now()
	stream := GetMockStream()
	ch = make(chan *Tweet, 1)
	// Producer
	go producer(stream)

	// Consumer
	go consumer()

	fmt.Printf("Process took %s\n", time.Since(start))
}//TEACHER! I provided second way of solving this task, could not check if they work or not. Cause it does not compile
//for some reason. Can you consider that answer if first is wrong, please?! I get this error:
//# command-line-arguments
//.\main.go:66:14: undefined: Tweet
//.\main.go:67:22: undefined: Stream
//.\main.go:70:13: undefined: ErrEOF
//.\main.go:94:12: undefined: GetMockStream
//.\main.go:95:18: undefined: Tweet

//func producer(stream Stream) (chan *Tweet) {
//	ch := make(chan *Tweet, 2)
//	go func(out chan *Tweet) {
//		tweet, err := stream.Next()
//		if err == ErrEOF {
//			close(ch)
//			return ch
//		}
//		out <- tweet
//	}(ch)
//	return ch
//}
//
//func consumer(tweets chan *Tweet) {
//	go func() {
//		t := <- tweets
//		if t.IsTalkingAboutGo() {
//			fmt.Println(t.Username, "\ttweets about golang")
//		} else{
//			fmt.Println(t.Username, "\tdoes not tweet about golang")
//			}
//	}()
//}
//
//func main() {
//	start := time.Now()
//	stream := GetMockStream()
//
//
//	tweets := producer(stream)
//
//	// Consumer
//	consumer(tweets)
//
//	fmt.Printf("Process took %s\n", time.Since(start))
//}